package SonarResults;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {

    /*optional approach to be more flexible with the universalIssue Method hence this enables the user to quickly change
    the prefixURL*/
    static String prefixURL = "{URL}/api/issues/search?pageSize=500&componentKeys=";


    public static void main(String[] args) throws IOException {

        //Use the URL you want to test for Issues (Blocker,critical,major,minor) as a String parameter
        getBlockerIssues("http://{URL}/api/issues/search?pageSize=500&componentKeys={PROJECT_KEY}&severities=BLOCKER");
        universalIssues("{KEY}", "{ISSUE_TYPE}");
        qualityGates("{URL}/api/qualitygates/project_status?projectKey={PROJECT_KEY}");
    }


    //Method to get minor issues
    public static void getMinorIssues(String x) throws IOException {
        try {
            System.out.println("\n------MINOR-Issues------");
            //Set up the url you want to ping
            String urlToConnect = x;
            URL get = new URL(urlToConnect);
            HttpURLConnection con = (HttpURLConnection) get.openConnection();
            System.out.println("\nSending 'GET' request to URL : " + urlToConnect);

            //Why do we need this? For the JsonFile?
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputline;
            StringBuffer response = new StringBuffer();
            while ((inputline = in.readLine()) != null) {
                response.append(inputline);
            }
            //closes the BufferReader
            in.close();

            //Create a new JSONObject so we can search explicitly for Strings
            JSONObject res = new JSONObject(response.toString());   //the parameter is the response from the URL

            //Initialized an array so we can iterate through it to get a list of all the issues
            JSONArray issues = new JSONArray(res.getJSONArray("issues").toString());
            System.out.println("There are currently: " + issues.length() + " MINOR-Issues!");

            //Get the description of the problem and the level of the issue plus the line if available
            for (int i = 0; i < issues.length(); i++) {
                if (issues.getJSONObject(i).has("textRange")) {
                    int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                    int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type: " + type);
                } else {
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                }
            }

            //So we get a message what the error was and we can fix it directly
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    //Method to get blocker issues
    public static void getBlockerIssues(String x) throws IOException {
        try {
            System.out.println("\n------BLOCKER-Issues------");
            //Set up the url you want to ping
            String urlToConnect = x;
            URL get = new URL(urlToConnect);
            HttpURLConnection con = (HttpURLConnection) get.openConnection();

            System.out.println("\nSending 'GET' request to URL : " + urlToConnect);

            //Why do we need this? For the JsonFile?
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputline;
            StringBuffer response = new StringBuffer();
            while ((inputline = in.readLine()) != null) {
                response.append(inputline);
            }
            //closes the BufferReader
            in.close();

            //Create a new JSONObject so we can search explicitly for Strings
            JSONObject res = new JSONObject(response.toString());   //the parameter is the response from the URL

            //Initialized an array so we can iterate through it to get a list of all the issues
            JSONArray issues = new JSONArray(res.getJSONArray("issues").toString());
            System.out.println("There are currently: " + issues.length() + " BLOCKER-Issues!");

            //Get the description of the problem and the level of the issue plus the line if available
            for (int i = 0; i < issues.length(); i++) {
                if (issues.getJSONObject(i).has("textRange")) {
                    int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                    int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type: " + type);
                } else {
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                }
            }

            //So we get a message what the error was and we can fix it directly
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Method to get critical issues
    public static void getCriticalIssues(String x) throws IOException {
        try {
            System.out.println("\n------CRITICAL-Issues------");
            //Set up the url you want to ping
            String urlToConnect = x;
            URL get = new URL(urlToConnect);
            HttpURLConnection con = (HttpURLConnection) get.openConnection();
            System.out.println("\nSending 'GET' request to URL : " + urlToConnect);

            //Why do we need this? For the JsonFile?
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputline;
            StringBuffer response = new StringBuffer();
            while ((inputline = in.readLine()) != null) {
                response.append(inputline);
            }
            //closes the BufferReader
            in.close();

            //Create a new JSONObject so we can search explicitly for Strings
            JSONObject res = new JSONObject(response.toString());   //the parameter is the response from the URL

            //Initialized an array so we can iterate through it to get a list of all the issues
            JSONArray issues = new JSONArray(res.getJSONArray("issues").toString());
            System.out.println("There are currently: " + issues.length() + " CRITICAL-Issues!");

            //Get the description of the problem and the level of the issue plus the line if available
            for (int i = 0; i < issues.length(); i++) {
                if (issues.getJSONObject(i).has("textRange")) {
                    int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                    int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type " + type);
                } else {
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                }
            }

            //So we get a message what the error was and we can fix it directly
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    //Method to get major issues
    public static void getMajorIssues(String x) throws IOException {
        try {
            System.out.println("\n------MAJOR-Issues------");
            //Set up the url you want to ping
            String urlToConnect = x;
            URL get = new URL(urlToConnect);
            HttpURLConnection con = (HttpURLConnection) get.openConnection();
            System.out.println("\nSending 'GET' request to URL : " + urlToConnect);

            //Why do we need this? For the JsonFile?
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputline;
            StringBuffer response = new StringBuffer();
            while ((inputline = in.readLine()) != null) {
                response.append(inputline);
            }

            //closes the BufferReader
            in.close();

            //Create a new JSONObject so we can search explicitly for Strings
            JSONObject res = new JSONObject(response.toString());   //the parameter is the response from the URL

            //Initialized an array so we can iterate through it to get a list of all the issues
            JSONArray issues = new JSONArray(res.getJSONArray("issues").toString());
            System.out.println("There are currently: " + issues.length() + " MAJOR-Issues!");

            //Get the description of the problem and the level of the issue plus the line if available
            for (int i = 0; i < issues.length(); i++) {
                if (issues.getJSONObject(i).has("textRange")) {
                    int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                    int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type: " + type);
                } else {
                    String des = issues.getJSONObject(i).getString("message");
                    String type = issues.getJSONObject(i).getString("type");
                    System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                }
            }

            //So we get a message what the error was and we can fix it directly
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Method to get qualitygates
    public static void qualityGates(String x) throws IOException {
        try {
            System.out.println("\n------QualityGates------");
            URL toConnect = new URL(x);
            HttpURLConnection con = (HttpURLConnection) toConnect.openConnection();
            System.out.println("Sending 'GET' request to URL : " + x);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            //Cast the JSON-File to a JSONObject
            JSONObject res = new JSONObject(response.toString());
            JSONArray gates = new JSONArray(res.getJSONObject("projectStatus").getJSONArray("conditions").toString());
            JSONObject test = new JSONObject(res.getJSONObject("projectStatus").toString());

            System.out.println("\nThe current Project-Status is: " + test.get("status") + "\n");

            for (int i = 0; i < gates.length(); i++) {
                String status = gates.getJSONObject(i).getString("status");
                String metric = gates.getJSONObject(i).getString("metricKey");
                System.out.println("Status: " + status + " | Metric: " + metric);

            }

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /*Another method which only requires the project key and the issue type which should be displayed. We can use the
    * approach with a static string as class object for the prefixURL or we can just make a fix prefixURL if it is always
    the same sonarqubeURL */

    //This method does everything like the methods above just in one method. The only parameter are the PROJECT_KEY and ISSUE_TYPE
    public static void universalIssues(String key, String issueType) throws IOException {
        try {
            //Set up the url you want to ping
            String checkPages = Main.prefixURL + key;
            String urlToConnect = Main.prefixURL + key + "&severities=" + issueType;
            URL get = new URL(urlToConnect);
            URL take = new URL(checkPages);

            HttpURLConnection con = (HttpURLConnection) get.openConnection();
            HttpURLConnection con2 = (HttpURLConnection) take.openConnection();

            System.out.println("\nSending 'GET' request to URL : " + urlToConnect);

            //Why do we need this? For the JsonFile?
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputline;
            StringBuffer response = new StringBuffer();
            while ((inputline = in.readLine()) != null) {
                response.append(inputline);
            }
            //closes the BufferReader
            in.close();


            //For the second request
            BufferedReader in2 = new BufferedReader(
                    new InputStreamReader(con2.getInputStream()));
            String inputline2;
            StringBuffer response2 = new StringBuffer();
            while ((inputline2 = in2.readLine()) != null) {
                response2.append(inputline2);
            }
            //closes the BufferReader
            in2.close();

            /* Currently working to iterate through every JSON-Page if there are more
            JSONObject paging = new JSONObject(response2.toString());
            int pagingNumber = paging.getInt("paging");

            System.out.println("Pages: " + pagingNumber);*/

            //Create a new JSONObject so we can search explicitly for Strings
            JSONObject res = new JSONObject(response.toString());   //the parameter is the response from the URL

            //Initialized an array so we can iterate through it to get a list of all the issues
            JSONArray issues = new JSONArray(res.getJSONArray("issues").toString());

            if (issueType.equals("BLOCKER")) {
                System.out.println("There are currently: " + issues.length() + " BLOCKER-Issues!");

                //Get the description of the problem and the level of the issue plus the line if available
                for (int i = 0; i < issues.length(); i++) {
                    if (issues.getJSONObject(i).has("textRange")) {
                        int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                        int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type: " + type);
                    } else {
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                    }
                }
            } else if (issueType.equals("CRITICAL")) {
                System.out.println("There are currently: " + issues.length() + " CRITICAL-Issues!");

                //Get the description of the problem and the level of the issue plus the line if available
                for (int i = 0; i < issues.length(); i++) {
                    if (issues.getJSONObject(i).has("textRange")) {
                        int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                        int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type " + type);
                    } else {
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                    }
                }
            } else if (issueType.equals("MINOR")) {
                System.out.println("There are currently: " + issues.length() + " MINOR-Issues!");

                //Get the description of the problem and the level of the issue plus the line if available
                for (int i = 0; i < issues.length(); i++) {
                    if (issues.getJSONObject(i).has("textRange")) {
                        int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                        int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type: " + type);
                    } else {
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                    }
                }
            } else if (issueType.equals("MAJOR")) {
                System.out.println("There are currently: " + issues.length() + " MAJOR-Issues!");

                for (int i = 0; i < issues.length(); i++) {
                    if (issues.getJSONObject(i).has("textRange")) {
                        int a = issues.getJSONObject(i).getJSONObject("textRange").getInt("startLine");
                        int b = issues.getJSONObject(i).getJSONObject("textRange").getInt("endLine");
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Line: " + a + "-" + b + ". Type: " + type);
                    } else {
                        String des = issues.getJSONObject(i).getString("message");
                        String type = issues.getJSONObject(i).getString("type");
                        System.out.println(i + 1 + ". Description: " + des + " Type: " + type);
                    }
                }
            } else {
                System.out.println("invalid Type input, pleaser enter: BLOCKER,CRITICAL,MAJOR,MINOR as issue-type!");
            }

            //So we get a message what the error was and we can fix it directly
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}